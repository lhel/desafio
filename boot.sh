#!/usr/bin/env bash
set -e

function usage {
    echo "Usage :  $0 [commands] [--]
    Commands:
        run      Run a flask application by development server
        test     Run tests"
}
function run {
    flask run
}

function teste {
    pytest
}

TARGET=$1
case $TARGET in
  "help" )
    usage
  ;;
  "run" )
    run
  ;;
  "test" )
    teste
  ;;
  *)
   echo "Unknown command '${TARGET}'"
   usage
   exit 1
  ;;
esac
