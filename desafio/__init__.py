from flask import Flask

from desafio import api


app = Flask(__name__)

app.register_blueprint(api.bp)

from desafio.errors import *
