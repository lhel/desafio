@ECHO OFF

if "%1" == "build_app_image" (
    call :build_app_image
    goto end
)

if "%1" == "run_app" (
    call :run_app
    goto end
)

if "%1" == "test_app" (
    call :test_app
    goto end
)

if "%1" == "stop_app" (
    call :stop_app
    goto end
)

:build_app_image
docker build --no-cache -t desafio .
EXIT /B 0

:run_app
docker run -d -p 8000:5000 --name flask_app desafio && echo Server running in localhost:8000
EXIT /B 0

:test_app
docker run --rm --name flask_test desafio pytest
EXIT /B 0

:stop_app
docker stop flask_app && docker rm flask_app
EXIT /B 0

:end
