from flask import json, make_response
from jsonschema import ValidationError
from werkzeug.exceptions import HTTPException

from desafio import app


@app.errorhandler(HTTPException)
def handle_http_exception(e):
    """Return JSON instead of HTML for HTTP errors"""
    response = e.get_response()
    response.data = json.dumps({
        "code": e.code,
        "name": e.name,
        "description": e.description,
    })
    response.content_type = "application/json"
    return response


@app.errorhandler(ValidationError)
def handle_validation_exception(e):
    """Return JSON response when occurs an validation error in json payload"""
    response = make_response()
    response.data = json.dumps({
        "code": 422,
        "name": "Validation error",
        "description": e.message,
    })
    response.content_type = "application/json"
    return response
