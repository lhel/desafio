from datetime import datetime


def compute_skill_experience(prof_expcs: list) -> list:
    """This function compute the duration in months that the freelancer
    worked with each skill.

    :prof_expcs: A list of dicts with professional experience data.
    :returns: A dict with information about the duration in months
    with each skill.

    """
    results = []
    for exp in prof_expcs:
        months = diff_months(exp['startDate'], exp['endDate'])
        for skill in exp['skills']:
            try:
                skill_existed = [result for result in results
                                 if skill['id'] == result['id']][0]
            except IndexError:
                skill_existed = None
            if skill_existed:
                months_overlay = sum_months_overlap(skill_existed['id'], exp,
                                                    prof_expcs)
                skill_existed['durationInMonths'] += months - months_overlay
                continue
            results.append({'id': skill['id'], 
                            'name': skill['name'],
                            'durationInMonths': months})
    return sorted(results, key=lambda computed_skill: computed_skill['id'])


def diff_months(start_date: datetime, end_date: datetime) -> int:
    """Calculate the quantity of months between a start date and end date.

    :start_date: A datetime instance.
    :end_date: A datetime instance.
    :returns: The quantity of months.

    """
    return (end_date.year - start_date.year) * 12 + (end_date.month - start_date.month)


def compute_months_overlap(first_range: tuple, second_range: tuple) -> int:
    """This function compute the number of months of overlap, if the delta
    is positive.

    :first_range: A tuple with start date and start end.
    :second_range: A tuple with start date and start end.
    :returns: A int value of number of months of overlap.

    """
    latest_start = max(first_range[0], second_range[0])
    earliest_end = min(first_range[1], second_range[1])
    delta = (earliest_end - latest_start).days + 1
    overlap = max(0, delta)
    return round(overlap / 30) if overlap > 0 else 0


def sum_months_overlap(skill_id: int, prof_exp: dict,
                       prof_expcs: list) -> int:
    """This function compute the sum of months overlap for each experience in
    one skill.

    :skill_id: A skill id integer.
    :prof_exp: A professional experience as dict.
    :returns: An int that represents the sum of months overlap.

    """
    result = 0
    prev_expcs = [prev_exc for prev_exc in prof_expcs
                  if prev_exc['endDate'] > prof_exp['endDate']]
    for expc in prev_expcs:
        if [skill['id'] == skill_id for skill in expc['skills']]:
            r1 = prof_exp['startDate'], prof_exp['endDate']
            r2 = expc['startDate'], expc['endDate']
            result += round(compute_months_overlap(r1, r2))
    return result
