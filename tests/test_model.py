import pytest

from datetime import datetime

from desafio.model import (diff_months, compute_months_overlap,
                           sum_months_overlap, compute_skill_experience)


@pytest.mark.parametrize("start_date,end_date,expected", [
    (datetime(2018, 3, 1), datetime(2018, 3, 1), 0),
    (datetime(2018, 3, 1), datetime(2018, 4, 1), 1),
    (datetime(2018, 3, 1), datetime(2019, 2, 1), 11),
    (datetime(2016, 3, 1), datetime(2018, 5, 1), 26),
])
def test_diff_month(start_date, end_date, expected):
    assert diff_months(start_date, end_date) == expected


@pytest.mark.parametrize("first_range,second_range,expected", [
    ((datetime(2016, 1, 1), datetime(2018, 5, 1)),
     (datetime(2014, 1, 1), datetime(2016, 9, 1)), 8),
    ((datetime(2014, 1, 1), datetime(2016, 9, 1)),
     (datetime(2013, 5, 1), datetime(2014, 7, 1)), 6),
    ((datetime(2013, 5, 1), datetime(2014, 7, 1)),
     (datetime(2016, 1, 1), datetime(2018, 5, 1)), 0),
])
def test_compute_months_overlap(first_range, second_range, expected):
    assert compute_months_overlap(first_range, second_range) == expected


@pytest.mark.parametrize("skill_id,prof_exp,prof_expcs,expected", [
    (370, 
     {
       "id": 54,
       "companyName": "Hayes - Veum",
       "startDate": datetime.fromisoformat("2014-01-01T00:00:00+01:00"),
       "endDate": datetime.fromisoformat("2016-09-01T00:00:00+01:00"),
       "skills": [
           {
               "id": 470,
               "name": "MySQL"
           },
           {
               "id": 400,
               "name": "Java"
           },
           {
                "id": 370,
                "name": "Javascript"
           }
       ]
     },
     [
         {
             'companyName': 'Okuneva, Kerluke and Strosin',
             'endDate': datetime.fromisoformat('2018-05-01T00:00:00+01:00'),
             'id': 4,
             'skills': [{'id': 241, 'name': 'React'},
                        {'id': 270, 'name': 'Node.js'},
                        {'id': 370, 'name': 'Javascript'}],
             'startDate': datetime.fromisoformat('2016-01-01T00:00:00+01:00')},
         {
           "id": 54,
           "companyName": "Hayes - Veum",
           "startDate": datetime.fromisoformat("2014-01-01T00:00:00+01:00"),
           "endDate": datetime.fromisoformat("2016-09-01T00:00:00+01:00"),
           "skills": [
               {
                   "id": 470,
                   "name": "MySQL"
               },
               {
                   "id": 400,
                   "name": "Java"
               },
               {
                    "id": 370,
                    "name": "Javascript"
               }
           ]
         },
     ], 8),
    (370,
     {
       "id": 54,
       "companyName": "Hayes - Veum",
       "startDate": datetime.fromisoformat("2014-01-01T00:00:00+01:00"),
       "endDate": datetime.fromisoformat("2016-09-01T00:00:00+01:00"),
       "skills": [
           {
               "id": 470,
               "name": "MySQL"
           },
           {
               "id": 400,
               "name": "Java"
           },
           {
                "id": 370,
                "name": "Javascript"
           }
       ]
     },
     [
         {
           "id": 54,
           "companyName": "Hayes - Veum",
           "startDate": datetime.fromisoformat("2014-01-01T00:00:00+01:00"),
           "endDate": datetime.fromisoformat("2016-09-01T00:00:00+01:00"),
           "skills": [
               {
                   "id": 470,
                   "name": "MySQL"
               },
               {
                   "id": 400,
                   "name": "Java"
               },
               {
                    "id": 370,
                    "name": "Javascript"
               }
           ]
         },
     ], 0),
])
def test_sum_months_overlay(skill_id, prof_exp, prof_expcs, expected):
    assert sum_months_overlap(skill_id, prof_exp, prof_expcs) == expected


@pytest.mark.parametrize("prof_expcs, expected", [
    (
        [
            {'companyName': 'Okuneva, Kerluke and Strosin',
             'endDate': datetime.fromisoformat('2018-05-01T00:00:00+01:00'),
             'id': 4,
             'skills': [{'id': 241, 'name': 'React'},
                        {'id': 270, 'name': 'Node.js'},
                        {'id': 370, 'name': 'Javascript'}],
             'startDate': datetime.fromisoformat('2016-01-01T00:00:00+01:00')},
        ],
        [{'durationInMonths': 28, 'id': 241, 'name': 'React'},
         {'durationInMonths': 28, 'id': 270, 'name': 'Node.js'},
         {'durationInMonths': 28, 'id': 370, 'name': 'Javascript'}],
    ),
    (
        [
            {'companyName': 'Okuneva, Kerluke and Strosin',
             'endDate': datetime.fromisoformat('2018-05-01T00:00:00+01:00'),
             'id': 4,
             'skills': [{'id': 241, 'name': 'React'},
                        {'id': 270, 'name': 'Node.js'},
                        {'id': 370, 'name': 'Javascript'}],
             'startDate': datetime.fromisoformat('2016-01-01T00:00:00+01:00')},
            {
             "id": 54,
             "companyName": "Hayes - Veum",
             "startDate": datetime.fromisoformat("2014-01-01T00:00:00+01:00"),
             "endDate": datetime.fromisoformat("2016-09-01T00:00:00+01:00"),
             "skills": [
                {
                    "id": 470,
                    "name": "MySQL"
                },
                {
                    "id": 400,
                    "name": "Java"
                },
                {
                    "id": 370,
                    "name": "Javascript"
                }]
            }
        ],
        [{'durationInMonths': 28, 'id': 241, 'name': 'React'},
         {'durationInMonths': 28, 'id': 270, 'name': 'Node.js'},
         {'durationInMonths': 52, 'id': 370, 'name': 'Javascript'},
         {'durationInMonths': 32, 'id': 400, 'name': 'Java'},
         {'durationInMonths': 32, 'id': 470, 'name': 'MySQL'}],
    ),
    (
        [
            {'companyName': 'Okuneva, Kerluke and Strosin',
             'endDate': datetime.fromisoformat('2018-05-01T00:00:00+01:00'),
             'id': 4,
             'skills': [{'id': 241, 'name': 'React'},
                        {'id': 270, 'name': 'Node.js'},
                        {'id': 370, 'name': 'Javascript'}],
             'startDate': datetime.fromisoformat('2016-01-01T00:00:00+01:00')},
            {
                "id": 54,
                "companyName": "Hayes - Veum",
                "startDate": datetime.fromisoformat("2014-01-01T00:00:00+01:00"),
                "endDate": datetime.fromisoformat("2016-09-01T00:00:00+01:00"),
                "skills": [
                    {
                        "id": 470,
                        "name": "MySQL"
                    },
                    {
                        "id": 400,
                        "name": "Java"
                    },
                    {
                        "id": 370,
                        "name": "Javascript"
                    }]
            },
            {
               "id": 80,
               "companyName": "Harber, Kirlin and Thompson",
               "startDate": datetime.fromisoformat("2013-05-01T00:00:00+01:00"),
               "endDate": datetime.fromisoformat("2014-07-01T00:00:00+01:00"),
               "skills": [
                   {
                       "id": 370,
                       "name": "Javascript"
                   },
                   {
                       "id": 400,
                       "name": "Java"
                   }
               ]
            },
        ],
        [{'durationInMonths': 28, 'id': 241, 'name': 'React'},
         {'durationInMonths': 28, 'id': 270, 'name': 'Node.js'},
         {'durationInMonths': 60, 'id': 370, 'name': 'Javascript'},
         {'durationInMonths': 40, 'id': 400, 'name': 'Java'},
         {'durationInMonths': 32, 'id': 470, 'name': 'MySQL'}],
    ),
])
def test_compute_skill_experience(prof_expcs, expected):
    assert compute_skill_experience(prof_expcs) == expected
