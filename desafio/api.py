from datetime import datetime

from flask import Blueprint, request, jsonify
from jsonschema import validate

from desafio.model import compute_skill_experience
from desafio.validations import schemas

bp = Blueprint('api', __name__, url_prefix='/api')


@bp.route('/calc_exp_by_months', methods=['POST'])
def calc_exp_by_months():
    """Endpoint that receives a payload JSON  and compute 
    the duration in months that the freelancer worked with each skill.

    :returns: A 200 response and a JSON data with each skill and 
    experience in months from freelancer.

    """
    validate(instance=request.json, schema=schemas.freelance)
    prof_expcs = request.json['freelance']['professionalExperiences']

    for prof_exp in prof_expcs:
        prof_exp['startDate'] = datetime.fromisoformat(prof_exp['startDate'])
        prof_exp['endDate'] = datetime.fromisoformat(prof_exp['endDate'])

    computed_skills = compute_skill_experience(prof_expcs)

    return {'freelance': {'id': request.json['freelance']['id'],
                          'computedSkills': computed_skills}}, 200
