import pytest

from pprint import pprint

from datetime import datetime


@pytest.mark.parametrize("expected", [
    ({
        "freelance": {
            "id": 42,
            "computedSkills": [
                {
                    "id": 241,
                    "name": "React",
                    "durationInMonths": 28
                },
                {
                    "id": 270,
                    "name": "Node.js",
                    "durationInMonths": 28
                },
                {
                    "id": 370,
                    "name": "Javascript",
                    "durationInMonths": 60
                },
                {
                    "id": 400,
                    "name": "Java",
                    "durationInMonths": 40
                },
                {
                    "id": 470,
                    "name": "MySQL",
                    "durationInMonths": 32
                }
            ]
        }
    })
])
def test_calc_skills_by_months(client, json_payload, expected):
    resp = client.post('/api/calc_exp_by_months', json=json_payload)
    json_data = resp.get_json()
    assert json_data == expected


def test_calc_skills_by_months_validation_error(client, json_payload):
    json_payload['freelance'].pop('user')
    resp = client.post('/api/calc_exp_by_months', json=json_payload)
    json_data = resp.get_json()
    pprint(json_data)
    assert json_data['code'] == 422
