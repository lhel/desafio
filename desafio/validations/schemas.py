freelance = {
    "$schema": "http://json-schema.org/draft-07/schema",
    "type": "object",
    "default": {},
    "examples": [
        {
            "freelance": {
                "id": 42,
                "user": {
                    "firstName": "Hunter",
                    "lastName": "Moore",
                    "jobTitle": "Fullstack JS Developer"
                },
                "status": "new",
                "retribution": 650,
                "availabilityDate": "2018-06-13T00:00:00+01:00",
                "professionalExperiences": [
                    {
                        "id": 4,
                        "companyName": "Okuneva, Kerluke and Strosin",
                        "startDate": "2016-01-01T00:00:00+01:00",
                        "endDate": "2018-05-01T00:00:00+01:00",
                        "skills": [
                            {
                                "id": 241,
                                "name": "React"
                            },
                            {
                                "id": 270,
                                "name": "Node.js"
                            },
                            {
                                "id": 370,
                                "name": "Javascript"
                            }
                        ]
                    },
                    {
                        "id": 54,
                        "companyName": "Hayes - Veum",
                        "startDate": "2014-01-01T00:00:00+01:00",
                        "endDate": "2016-09-01T00:00:00+01:00",
                        "skills": [
                            {
                                "id": 470,
                                "name": "MySQL"
                            },
                            {
                                "id": 400,
                                "name": "Java"
                            },
                            {
                                "id": 370,
                                "name": "Javascript"
                            }
                        ]
                    },
                    {
                        "id": 80,
                        "companyName": "Harber, Kirlin and Thompson",
                        "startDate": "2013-05-01T00:00:00+01:00",
                        "endDate": "2014-07-01T00:00:00+01:00",
                        "skills": [
                            {
                                "id": 370,
                                "name": "Javascript"
                            },
                            {
                                "id": 400,
                                "name": "Java"
                            }
                        ]
                    }
                ]
            }
        }
    ],
    "required": [
        "freelance"
    ],
    "properties": {
        "freelance": {
            "$id": "#/properties/freelance",
            "type": "object",
            "title": "The freelance schema",
            "default": {},
            "examples": [
                {
                    "id": 42,
                    "user": {
                        "firstName": "Hunter",
                        "lastName": "Moore",
                        "jobTitle": "Fullstack JS Developer"
                    },
                    "status": "new",
                    "retribution": 650,
                    "availabilityDate": "2018-06-13T00:00:00+01:00",
                    "professionalExperiences": [
                        {
                            "id": 4,
                            "companyName": "Okuneva, Kerluke and Strosin",
                            "startDate": "2016-01-01T00:00:00+01:00",
                            "endDate": "2018-05-01T00:00:00+01:00",
                            "skills": [
                                {
                                    "id": 241,
                                    "name": "React"
                                },
                                {
                                    "id": 270,
                                    "name": "Node.js"
                                },
                                {
                                    "id": 370,
                                    "name": "Javascript"
                                }
                            ]
                        },
                        {
                            "id": 54,
                            "companyName": "Hayes - Veum",
                            "startDate": "2014-01-01T00:00:00+01:00",
                            "endDate": "2016-09-01T00:00:00+01:00",
                            "skills": [
                                {
                                    "id": 470,
                                    "name": "MySQL"
                                },
                                {
                                    "id": 400,
                                    "name": "Java"
                                },
                                {
                                    "id": 370,
                                    "name": "Javascript"
                                }
                            ]
                        },
                        {
                            "id": 80,
                            "companyName": "Harber, Kirlin and Thompson",
                            "startDate": "2013-05-01T00:00:00+01:00",
                            "endDate": "2014-07-01T00:00:00+01:00",
                            "skills": [
                                {
                                    "id": 370,
                                    "name": "Javascript"
                                },
                                {
                                    "id": 400,
                                    "name": "Java"
                                }
                            ]
                        }
                    ]
                }
            ],
            "required": [
                "id",
                "user",
                "status",
                "retribution",
                "availabilityDate",
                "professionalExperiences"
            ],
            "properties": {
                "id": {
                    "$id": "#/properties/freelance/properties/id",
                    "type": "integer",
                    "title": "The id schema",
                    "default": 0,
                    "examples": [
                        42
                    ]
                },
                "user": {
                    "$id": "#/properties/freelance/properties/user",
                    "type": "object",
                    "title": "The user schema",
                    "default": {},
                    "examples": [
                        {
                            "firstName": "Hunter",
                            "lastName": "Moore",
                            "jobTitle": "Fullstack JS Developer"
                        }
                    ],
                    "required": [
                        "firstName",
                        "lastName",
                        "jobTitle"
                    ],
                    "properties": {
                        "firstName": {
                            "$id": "#/properties/freelance/properties/user/properties/firstName",
                            "type": "string",
                            "title": "The firstName schema",
                            "default": "",
                            "examples": [
                                "Hunter"
                            ]
                        },
                        "lastName": {
                            "$id": "#/properties/freelance/properties/user/properties/lastName",
                            "type": "string",
                            "title": "The lastName schema",
                            "default": "",
                            "examples": [
                                "Moore"
                            ]
                        },
                        "jobTitle": {
                            "$id": "#/properties/freelance/properties/user/properties/jobTitle",
                            "type": "string",
                            "title": "The jobTitle schema",
                            "default": "",
                            "examples": [
                                "Fullstack JS Developer"
                            ]
                        }
                    },
                    "additionalProperties": True
                },
                "status": {
                    "$id": "#/properties/freelance/properties/status",
                    "type": "string",
                    "title": "The status schema",
                    "default": "",
                    "examples": [
                        "new"
                    ]
                },
                "retribution": {
                    "$id": "#/properties/freelance/properties/retribution",
                    "type": "integer",
                    "title": "The retribution schema",
                    "default": 0,
                    "examples": [
                        650
                    ]
                },
                "availabilityDate": {
                    "$id": "#/properties/freelance/properties/availabilityDate",
                    "type": "string",
                    "title": "The availabilityDate schema",
                    "default": "",
                    "examples": [
                        "2018-06-13T00:00:00+01:00"
                    ]
                },
                "professionalExperiences": {
                    "$id": "#/properties/freelance/properties/professionalExperiences",
                    "type": "array",
                    "title": "The professionalExperiences schema",
                    "default": [],
                    "examples": [
                        [
                            {
                                "id": 4,
                                "companyName": "Okuneva, Kerluke and Strosin",
                                "startDate": "2016-01-01T00:00:00+01:00",
                                "endDate": "2018-05-01T00:00:00+01:00",
                                "skills": [
                                    {
                                        "id": 241,
                                        "name": "React"
                                    },
                                    {
                                        "id": 270,
                                        "name": "Node.js"
                                    },
                                    {
                                        "id": 370,
                                        "name": "Javascript"
                                    }
                                ]
                            },
                            {
                                "id": 54,
                                "companyName": "Hayes - Veum",
                                "startDate": "2014-01-01T00:00:00+01:00",
                                "endDate": "2016-09-01T00:00:00+01:00",
                                "skills": [
                                    {
                                        "id": 470,
                                        "name": "MySQL"
                                    },
                                    {
                                        "id": 400,
                                        "name": "Java"
                                    },
                                    {
                                        "id": 370,
                                        "name": "Javascript"
                                    }
                                ]
                            }
                        ]
                    ],
                    "additionalItems": True,
                    "items": {
                        "$id": "#/properties/freelance/properties/professionalExperiences/items",
                        "anyOf": [
                            {
                                "$id": "#/properties/freelance/properties/professionalExperiences/items/anyOf/0",
                                "type": "object",
                                "title": "The first anyOf schema",
                                "default": {},
                                "examples": [
                                    {
                                        "id": 4,
                                        "companyName": "Okuneva, Kerluke and Strosin",
                                        "startDate": "2016-01-01T00:00:00+01:00",
                                        "endDate": "2018-05-01T00:00:00+01:00",
                                        "skills": [
                                            {
                                                "id": 241,
                                                "name": "React"
                                            },
                                            {
                                                "id": 270,
                                                "name": "Node.js"
                                            },
                                            {
                                                "id": 370,
                                                "name": "Javascript"
                                            }
                                        ]
                                    }
                                ],
                                "required": [
                                    "id",
                                    "companyName",
                                    "startDate",
                                    "endDate",
                                    "skills"
                                ],
                                "properties": {
                                    "id": {
                                        "$id": "#/properties/freelance/properties/professionalExperiences/items/anyOf/0/properties/id",
                                        "type": "integer",
                                        "title": "The id schema",
                                        "default": 0,
                                        "examples": [
                                            4
                                        ]
                                    },
                                    "companyName": {
                                        "$id": "#/properties/freelance/properties/professionalExperiences/items/anyOf/0/properties/companyName",
                                        "type": "string",
                                        "title": "The companyName schema",
                                        "default": "",
                                        "examples": [
                                            "Okuneva, Kerluke and Strosin"
                                        ]
                                    },
                                    "startDate": {
                                        "$id": "#/properties/freelance/properties/professionalExperiences/items/anyOf/0/properties/startDate",
                                        "type": "string",
                                        "title": "The startDate schema",
                                        "default": "",
                                        "examples": [
                                            "2016-01-01T00:00:00+01:00"
                                        ]
                                    },
                                    "endDate": {
                                        "$id": "#/properties/freelance/properties/professionalExperiences/items/anyOf/0/properties/endDate",
                                        "type": "string",
                                        "title": "The endDate schema",
                                        "default": "",
                                        "examples": [
                                            "2018-05-01T00:00:00+01:00"
                                        ]
                                    },
                                    "skills": {
                                        "$id": "#/properties/freelance/properties/professionalExperiences/items/anyOf/0/properties/skills",
                                        "type": "array",
                                        "title": "The skills schema",
                                        "default": [],
                                        "examples": [
                                            [
                                                {
                                                    "id": 241,
                                                    "name": "React"
                                                },
                                                {
                                                    "id": 270,
                                                    "name": "Node.js"
                                                }
                                            ]
                                        ],
                                        "additionalItems": True,
                                        "items": {
                                            "$id": "#/properties/freelance/properties/professionalExperiences/items/anyOf/0/properties/skills/items",
                                            "anyOf": [
                                                {
                                                    "$id": "#/properties/freelance/properties/professionalExperiences/items/anyOf/0/properties/skills/items/anyOf/0",
                                                    "type": "object",
                                                    "title": "The first anyOf schema",
                                                    "default": {},
                                                    "examples": [
                                                        {
                                                            "id": 241,
                                                            "name": "React"
                                                        }
                                                    ],
                                                    "required": [
                                                        "id",
                                                        "name"
                                                    ],
                                                    "properties": {
                                                        "id": {
                                                            "$id": "#/properties/freelance/properties/professionalExperiences/items/anyOf/0/properties/skills/items/anyOf/0/properties/id",
                                                            "type": "integer",
                                                            "title": "The id schema",
                                                            "default": 0,
                                                            "examples": [
                                                                241
                                                            ]
                                                        },
                                                        "name": {
                                                            "$id": "#/properties/freelance/properties/professionalExperiences/items/anyOf/0/properties/skills/items/anyOf/0/properties/name",
                                                            "type": "string",
                                                            "title": "The name schema",
                                                            "default": "",
                                                            "examples": [
                                                                "React"
                                                            ]
                                                        }
                                                    },
                                                    "additionalProperties": True
                                                }
                                            ]
                                        }
                                    }
                                },
                                "additionalProperties": True
                            }
                        ]
                    }
                }
            },
            "additionalProperties": True
        }
    },
    "additionalProperties": True
}
