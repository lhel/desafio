import json

import pytest

from desafio import app


@pytest.fixture
def client():
    app.config['TESTING'] = True

    with app.test_client() as client:
        yield client


@pytest.fixture
def json_payload():
    with open('examples/freelancer.json') as f:
        return json.load(f)
