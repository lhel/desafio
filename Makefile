build_app_image:
	docker build --no-cache -t desafio .

run_app:
	docker run -d -p 8000:5000 --name flask_app desafio && echo Server running in localhost:8000

test_app:
	docker run --rm --name flask_test desafio pytest

stop_app:
	docker stop flask_app && docker rm flask_app
